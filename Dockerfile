FROM maven:3.8.6-eclipse-temurin-17-alpine AS builder
WORKDIR /project
COPY . .
RUN mvn package -DskipTests

FROM eclipse-temurin:17.0.4.1_1-jre-alpine
ARG JAR_FILE=/project/target/*.jar
WORKDIR /opt/app
COPY --from=builder ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","app.jar"]