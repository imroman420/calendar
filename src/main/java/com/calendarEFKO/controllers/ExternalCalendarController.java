package com.calendarEFKO.controllers;

import com.calendarEFKO.dto.CalDavDto;
import com.calendarEFKO.dto.ResultCalendar;
import com.calendarEFKO.exception.ErrorResponse;
import com.calendarEFKO.exception.NotCreatedObjectException;
import com.calendarEFKO.exception.NotFoundObjectException;
import com.calendarEFKO.exception.ValidException;
import com.calendarEFKO.models.Calendar;
import com.calendarEFKO.services.ExternalCalendarService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.fortuna.ical4j.data.ParserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/api/caldav")
@Tag(name = "CalDav", description = "Контроллер для работы со внешними календарями")
public class ExternalCalendarController {
    ExternalCalendarService externalCalendarService;

    @Autowired
    ExternalCalendarController(ExternalCalendarService externalCalendarService) {
        this.externalCalendarService = externalCalendarService;

    }


    @PostMapping
    @Operation(summary = "Добавление внешнего календаря")
    @PreAuthorize("hasRole('USER')")
    public ResultCalendar<Calendar> createCalDav(Authentication authentication, @RequestBody CalDavDto calDavDto) {
        return new ResultCalendar<Calendar>(externalCalendarService.addExternalCalendar(authentication, calDavDto.getName(), calDavDto.getColor(), calDavDto.getUrlCalDav()));
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(NotFoundObjectException e) {

        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }


    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(ParserException e) {

        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(IOException e) {
        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(ValidException e) {

        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(NotCreatedObjectException e) {

        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }


}
