package com.calendarEFKO.controllers;

import com.calendarEFKO.dto.ResultEventType;
import com.calendarEFKO.dto.ResultList;
import com.calendarEFKO.exception.ErrorResponse;
import com.calendarEFKO.exception.NotCreatedObjectException;
import com.calendarEFKO.exception.ValidException;
import com.calendarEFKO.models.EventType;
import com.calendarEFKO.services.EventTypeService;
import com.calendarEFKO.view.Views;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.calendarEFKO.exception.ErrorsUtil.returnErrorsToClient;

@RestController
@RequestMapping("/api/eventtype")
@Tag(
        name = "Типы ивентов",
        description = "Методы для работы с типами ивентов"
)
public class EventTypeController {
    private final EventTypeService eventTypeService;

    @Autowired
    public EventTypeController(EventTypeService eventTypeService) {
        this.eventTypeService = eventTypeService;
    }

    @GetMapping("/{id}")
    @Operation(summary = "Получить информацию о типе ивента")
    @PreAuthorize("hasRole('USER')")
    public ResultEventType<EventType> findEventType(Authentication authentication, @PathVariable int id) {
        return new ResultEventType<EventType>(eventTypeService.findOne(id));

    }

    @GetMapping
    @Operation(summary = "Получить информацию о всех типах ивентов")
    @PreAuthorize("hasRole('USER')")
    @JsonView(Views.Public.class)
    public ResultList<EventType> findAll(Authentication authentication) {
        return new ResultList<EventType>(eventTypeService.findAll());
    }

    @PostMapping
    @Operation(summary = "Создать тип ивента")
    @PreAuthorize("hasRole('USER')")
    public ResultEventType<EventType> createEventType(Authentication authentication, @RequestBody @Valid EventType eventType, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) returnErrorsToClient(bindingResult);
        return new ResultEventType<EventType>(eventTypeService.save(eventType));

    }

    @PutMapping("/{id}")
    @Operation(summary = "Измененить тип ивента")
    @PreAuthorize("hasRole('USER')")
    public ResultEventType<EventType> updateEventType(Authentication authentication, @RequestBody @Valid EventType eventType, BindingResult bindingResult, @PathVariable int id) {
        if (bindingResult.hasErrors()) returnErrorsToClient(bindingResult);
        return new ResultEventType<EventType>(eventTypeService.update(id, eventType));

    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удалить тип ивента")
    @PreAuthorize("hasRole('USER')")
    public ResultEventType<EventType> deleteUser(Authentication authentication, @PathVariable int id) {
        return new ResultEventType<EventType>(eventTypeService.delete(id));
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(ValidException e) {

        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(NotCreatedObjectException e) {

        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}
