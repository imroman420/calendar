package com.calendarEFKO.controllers;

import com.calendarEFKO.dto.ResultList;
import com.calendarEFKO.dto.ResultUser;
import com.calendarEFKO.exception.ErrorResponse;
import com.calendarEFKO.exception.NotCreatedObjectException;
import com.calendarEFKO.exception.NotFoundObjectException;
import com.calendarEFKO.exception.ValidException;
import com.calendarEFKO.models.User;
import com.calendarEFKO.services.UserService;
import com.calendarEFKO.view.Views;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.calendarEFKO.exception.ErrorsUtil.returnErrorsToClient;

@RestController
@RequestMapping("/api/users")
@Tag(name = "Пользователи", description = "Методы для работы с пользователями системы")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    @Operation(summary = "Аунтификация пользователя")
    @PreAuthorize("hasRole('USER')")
    public ResultUser<User> loginUser(Authentication authentication) {

        if (userService.findMe(authentication) != null){
            return new ResultUser<User>(userService.findMe(authentication));
        }
        else{
            return new ResultUser<User>(userService.saveMe(authentication));
        }
    }

    @GetMapping("/me")
    @Operation(summary = "Получить данные о 'моем' пользователе")
    @PreAuthorize("hasRole('USER')")
    public ResultUser<User> findMe(Authentication authentication) {
        return new ResultUser<User>(userService.findMe(authentication));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('USER')")
    @Operation(summary = "Получить информацию о пользователе")
    public ResultUser<User> getUser(Authentication authentication, @PathVariable String id) {
        return new ResultUser<User>(userService.findOne(id));
    }

    @GetMapping
    @PreAuthorize("hasRole('USER')")
    @Operation(summary = "Получить информацию о всех пользователях")
    @JsonView(Views.Public.class)
    public ResultList<User> findByAllParam(Authentication authentication,
                                           @RequestParam(value = "id", required = false) String id,
                                           @RequestParam(value = "login", required = false) String login,
                                           @RequestParam(value = "email", required = false) String email,
                                           @RequestParam(value = "firstName", required = false) String firstName,
                                           @RequestParam(value = "middleName", required = false) String middleName,
                                           @RequestParam(value = "lastName", required = false) String lastName,
                                           @RequestParam(value = "company", required = false) String company,
                                           @RequestParam(value = "department", required = false) String department,
                                           @RequestParam(value = "position", required = false) String position) {
        return new ResultList<User>(userService.findAll(id, login, email, firstName, middleName, lastName, company, department, position));
    }


    @PostMapping
    @PreAuthorize("hasRole('USER')")
    @Operation(summary = "Создать пользователя")
    public ResultUser<User> createUser(Authentication authentication, @RequestBody @Valid User user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) returnErrorsToClient(bindingResult);
        return new ResultUser<User>(userService.save(user));

    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('USER')")
    @Operation(summary = "Обновить пользователя")
    public ResultUser<User> updateUser(Authentication authentication, @RequestBody @Valid User user, BindingResult bindingResult, @PathVariable int id) {
        if (bindingResult.hasErrors()) returnErrorsToClient(bindingResult);
        return new ResultUser<User>(userService.update(id, user));

    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('USER')")
    @Operation(summary = "Удалить пользователя")
    public ResultUser<User> deleteUser(Authentication authentication, @PathVariable int id) {
        return new ResultUser<User>(userService.delete(id));
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(NotFoundObjectException e) {

        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(ValidException e) {

        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(NotCreatedObjectException e) {

        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }
}