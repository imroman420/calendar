package com.calendarEFKO.controllers;

import com.calendarEFKO.dto.ResultList;
import com.calendarEFKO.exception.ErrorResponse;
import com.calendarEFKO.exception.NotCreatedObjectException;
import com.calendarEFKO.exception.NotFoundObjectException;
import com.calendarEFKO.exception.ValidException;
import com.calendarEFKO.models.Event;
import com.calendarEFKO.services.EventService;
import com.calendarEFKO.view.Views;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@RestController
@RequestMapping("/api/weekday")
@Tag(name = "Ивенты", description = "Методы для работы с ивентами")
public class ProductionCalendarController {

    private final EventService eventService;

    @Autowired
    public ProductionCalendarController(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping
    @Operation(summary = "Получить информацию об ивенте")
    @PreAuthorize("hasRole('USER')")
    @JsonView(Views.Public.class)
    public ResultList<Event> findWeekdaysParams(Authentication authentication, @RequestParam(value = "date", required = false) LocalDate date, @RequestParam(value = "dtStart", required = false) LocalDate dtStart, @RequestParam(value = "dtEnd", required = false) LocalDate dtEnd) {
        return new ResultList<>(eventService.findWeekdaysParams(dtEnd, dtStart));
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(NotFoundObjectException e) {

        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(ValidException e) {

        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(NotCreatedObjectException e) {

        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

}
