package com.calendarEFKO.controllers;


import com.calendarEFKO.dto.EventDTO;
import com.calendarEFKO.dto.ResultEvent;
import com.calendarEFKO.dto.ResultList;
import com.calendarEFKO.exception.ErrorResponse;
import com.calendarEFKO.exception.NotCreatedObjectException;
import com.calendarEFKO.exception.NotFoundObjectException;
import com.calendarEFKO.exception.ValidException;
import com.calendarEFKO.models.Calendar;
import com.calendarEFKO.models.Event;
import com.calendarEFKO.services.EventService;
import com.calendarEFKO.view.Views;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

import static com.calendarEFKO.exception.ErrorsUtil.returnErrorsToClient;

@RestController
@RequestMapping("/api/event")
@Tag(name = "Ивенты", description = "Методы для работы с ивентами")
public class EventController {

    private final EventService eventService;

    @Autowired
    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping("/{id}")
    @Operation(summary = "Получить информацию об ивенте")
    @PreAuthorize("hasRole('USER')")
    public ResultEvent<Event> findEvent(Authentication authentication, @PathVariable int id) {
        return new ResultEvent<Event>(eventService.findOne(id));

    }

    @GetMapping
    @Operation(summary = "Получить информацию обо всех ивентах")
    @PreAuthorize("hasRole('USER')")
    public ResultList<EventDTO> FindEventByCalendar(Authentication authentication,
                                           @RequestParam(value = "name", required = false) String name,
                                           @RequestParam(value = "calendarId", required = false) Integer calendarId,
                                           @RequestParam(value = "eventTypeId", required = false) Integer eventTypeId,
                                           @RequestParam(value = "dtStart", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime dtStart,
                                           @RequestParam(value = "dtEnd", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime dtEnd)
    {
        return new ResultList<EventDTO>(eventService.findAll(name, calendarId, eventTypeId, dtStart, dtEnd));
    }

    @PostMapping
    @Operation(summary = "Создать ивент")
    @PreAuthorize("hasRole('USER')")
    public ResultEvent<Event> createEventByCalendars(Authentication authentication, @RequestBody EventDTO eventDto) {
        return new ResultEvent<Event>(eventService.save(eventDto));
    }


    @PutMapping("/{id}")
    @Operation(summary = "Изменить ивент")
    @PreAuthorize("hasRole('USER')")
    public ResultEvent<Event> updateEvent(Authentication authentication, @RequestBody @Valid EventDTO eventDTO, BindingResult bindingResult, @PathVariable int id) {
        if (bindingResult.hasErrors()) returnErrorsToClient(bindingResult);
        return new ResultEvent<Event>(eventService.updateEvent(id, eventDTO));
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удалить ивент")
    @PreAuthorize("hasRole('USER')")
    public ResultEvent<Event> deleteEvent(Authentication authentication, @PathVariable int id) {
        return new ResultEvent<Event>(eventService.deleteEvent(id));
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(NotFoundObjectException e) {

        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(ValidException e) {

        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(NotCreatedObjectException e) {

        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

}
