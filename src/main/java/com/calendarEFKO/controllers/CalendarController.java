package com.calendarEFKO.controllers;


import com.calendarEFKO.dto.ResultCalendar;
import com.calendarEFKO.dto.ResultList;
import com.calendarEFKO.exception.ErrorResponse;
import com.calendarEFKO.exception.NotCreatedObjectException;
import com.calendarEFKO.exception.NotFoundObjectException;
import com.calendarEFKO.exception.ValidException;
import com.calendarEFKO.models.Calendar;
import com.calendarEFKO.services.CalendarService;
import com.calendarEFKO.view.Views;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.calendarEFKO.exception.ErrorsUtil.returnErrorsToClient;

@RestController
@RequestMapping("/api/calendar")
@Tag(
        name = "Календари",
        description = "Методы для работы с календарями"
)
public class CalendarController {

    private final CalendarService calendarService;

    @Autowired
    public CalendarController(CalendarService calendarService) {
        this.calendarService = calendarService;
    }

    @GetMapping("/{id}")
    @Operation(summary = "Получить информацию о календаре")
    @PreAuthorize("hasRole('USER')")
    public ResultCalendar<Calendar> getEventByCalendarId(Authentication authentication, @PathVariable int id) {
        return new ResultCalendar<Calendar>(calendarService.findOne(id));
    }

    @GetMapping
    @Operation(summary = "Получить информацию о всех календарях")
    @PreAuthorize("hasRole('USER')")
    @JsonView(Views.Public.class)
    public ResultList<Calendar> findByAllParameters(Authentication authentication, @RequestParam(value = "name", required = false) String name, @RequestParam(value = "ownerId", required = false) String ownerId) {
        return new ResultList<Calendar>(calendarService.findAll(name, ownerId));
    }


    @PostMapping
    @Operation(summary = "Создать календарь")
    @PreAuthorize("hasRole('USER')")
    public ResultCalendar<Calendar> createCalendar(@RequestBody @Valid Calendar calendar, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) returnErrorsToClient(bindingResult);
        return new ResultCalendar<Calendar>(calendarService.save(calendar));
    }

    @PutMapping("/{id}")
    @Operation(summary = "Изменить календарь")
    @PreAuthorize("hasRole('USER')")
    public ResultCalendar<Calendar> updateCalendar(Authentication authentication, @RequestBody @Valid Calendar calendar, @PathVariable int id, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) returnErrorsToClient(bindingResult);
        return new ResultCalendar<Calendar>(calendarService.update(calendar, id));
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Удалить календарь")
    @PreAuthorize("hasRole('USER')")
    public ResultCalendar<Calendar> deleteCalendar(Authentication authentication, @PathVariable int id) {
        return new ResultCalendar<Calendar>(calendarService.delete(id));
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(NotFoundObjectException e) {

        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(ValidException e) {

        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler
    private ResponseEntity<ErrorResponse> handleException(NotCreatedObjectException e) {

        ErrorResponse response = new ErrorResponse(e.getMessage(), System.currentTimeMillis());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

}