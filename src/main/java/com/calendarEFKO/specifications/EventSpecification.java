package com.calendarEFKO.specifications;

import com.calendarEFKO.models.Calendar;
import com.calendarEFKO.models.Event;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.List;
import java.util.Objects;

public class EventSpecification implements Specification<Event> {
    private final SearchCriteria criteria;

    public EventSpecification(SearchCriteria criteria) {
        this.criteria = criteria;
    }

    public static Specification<Event> buildEventSpecification(List<SearchCriteria> searchCriteriaList) {
        Specification<Event> result = null;
        for (SearchCriteria searchCriteria : searchCriteriaList) {
            if (result == null) {
                result = Specification.where(new EventSpecification(searchCriteria));
            } else {
                result = result.and(new EventSpecification(searchCriteria));

            }
        }
        return result;
    }

    @Override
    public Predicate toPredicate(Root<Event> root, CriteriaQuery<?> query, CriteriaBuilder builder) {

        if (criteria.getOperation().equalsIgnoreCase(">")) {
            return builder.greaterThanOrEqualTo(root.get("dtStart"), criteria.getLocalDateTime());
        } else if (criteria.getOperation().equalsIgnoreCase("<")) {
            return builder.lessThan(root.get("dtEnd"), criteria.getLocalDateTime());
        } else if (criteria.getOperation().equalsIgnoreCase(":")) {
            if (root.get(criteria.getKey()).getJavaType() == String.class) {
                return builder.like(root.get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else {
                if (Objects.equals(criteria.getKey(), "calendars")) {
                    Join<Event, Calendar> authorsBook = root.join("calendars");
                    return builder.equal(authorsBook.get("id"), criteria.getValue());
                } else {
                    return builder.equal(root.get(criteria.getKey()), criteria.getValue());
                }
            }
        }
        return null;
    }

}