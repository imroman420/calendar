package com.calendarEFKO.specifications;

import com.calendarEFKO.models.Calendar;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.lang.NonNull;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

public class CalendarSpecification implements Specification<Calendar> {
    private final SearchCriteria criteria;
    public CalendarSpecification(SearchCriteria criteria) {
        this.criteria = criteria;
    }
    public static Specification<Calendar> buildCalendarSpecification(List<SearchCriteria> searchCriteriaList) {
        Specification<Calendar> result = null;
        for (SearchCriteria searchCriteria : searchCriteriaList) {
            if (result == null) {
                result = Specification.where(new CalendarSpecification(searchCriteria));
            } else {
                result = result.and(new CalendarSpecification(searchCriteria));

            }
        }
        return result;
    }

    @Override
    public Predicate toPredicate
            (@NonNull Root<Calendar> root, @NonNull CriteriaQuery<?> query, @NonNull CriteriaBuilder builder) {

        if (criteria.getOperation().equalsIgnoreCase(">")) {
            return builder.greaterThanOrEqualTo(
                    root.<String>get(criteria.getKey()), criteria.getValue().toString());
        } else if (criteria.getOperation().equalsIgnoreCase("<")) {
            return builder.lessThanOrEqualTo(
                    root.<String>get(criteria.getKey()), criteria.getValue().toString());
        } else if (criteria.getOperation().equalsIgnoreCase(":")) {
            if (root.get(criteria.getKey()).getJavaType() == String.class) {
                return builder.like(
                        root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
            } else {
                return builder.equal(root.get(criteria.getKey()), criteria.getValue());
            }
        }
        return null;
    }

}