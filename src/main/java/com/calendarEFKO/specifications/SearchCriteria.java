package com.calendarEFKO.specifications;

import java.time.LocalDateTime;

public class SearchCriteria {

    private String key;
    private String operation;
    private Object value;
    private LocalDateTime localDateTime;


    public SearchCriteria() {

    }

    public SearchCriteria(String key, String operation, Object value, LocalDateTime localDateTime) {
        this.key = key;
        this.operation = operation;
        this.value = value;
        this.localDateTime = localDateTime;
    }

    public String getKey() {
        return key;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(final String operation) {
        this.operation = operation;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(final Object value) {
        this.value = value;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }
}