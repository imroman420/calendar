package com.calendarEFKO.repositories;

import com.calendarEFKO.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {

    User findById(String id);

    @Query(value = "select * from users u where (?1 is null or u.id =?1) and (?2 is null or u.login =?2) and (?3 is null or u.email =?3) and (?4 is null or u.first_name =?4) and (?5 is null or u.middle_name =?5) and (?6 is null or u.last_name =?6) and u.dt_deletion is null", nativeQuery = true)
    List<User> findByAllParam(@Param("id") String id, @Param("login") String login, @Param("email") String email, @Param("firstName") String firstName, @Param("middleName") String middleName, @Param("lastName") String lastName);
}