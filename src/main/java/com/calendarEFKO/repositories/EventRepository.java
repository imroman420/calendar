package com.calendarEFKO.repositories;

import com.calendarEFKO.models.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface EventRepository extends JpaRepository<Event, Integer>, JpaSpecificationExecutor<Event> {
    @Query(value = "SELECT * FROM event e LEFT JOIN event_link_calendar ON e.id = event_link_calendar.event_id LEFT JOIN calendar ON calendar.id = event_link_calendar.calendar_id WHERE calendar.id = ?1 AND e.dt_start >= ?2 AND e.dt_end <= ?3 AND e.dt_deletion IS NULL", nativeQuery = true)
    List<Event> findEventsProductionCalendar(@Param("calendarId") Integer calendarId, @Param("dtStart") LocalDateTime dtStart, @Param("dtEnd") LocalDateTime dtEnd);

}
