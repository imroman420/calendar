package com.calendarEFKO.services;

import com.calendarEFKO.dto.EventDTO;
import com.calendarEFKO.models.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class IsDayOfExtractorService {

    private final EventService eventService;

    @Autowired
    public IsDayOfExtractorService(EventService eventService) {
        this.eventService = eventService;
    }

    @Scheduled(fixedDelay = 60000000)
    public void startAddToProductCalendar() throws InterruptedException {
        completionProductionCalendar(requestIsDayOff());
    }

    public Map<Integer, String> requestIsDayOff() {
        int thisYear = LocalDate.now().getYear();
        int yearStart = thisYear - 1;
        int yearEnd = thisYear + 2;
        RestTemplate restTemplate = new RestTemplate();
        Map<Integer, String> daysInYear = new HashMap<>();
        while (yearStart < yearEnd) {
            try {
                String response = restTemplate.getForEntity("https://isdayoff.ru/api/getdata" + "?" + "year=" + yearStart + "&" + "pre=1&covid=1", String.class).getBody();
                daysInYear.put(yearStart, String.valueOf(response));
                yearStart++;
            } catch (MissingResourceException e) {
                System.err.println(e.getMessage());
            }
        }
        return daysInYear;
    }
    @Transactional
    public void completionProductionCalendar(Map<Integer, String> isDayOff) throws InterruptedException {
        for (Map.Entry<Integer, String> oneYear : isDayOff.entrySet()) {
            int n = 0;
            Set<Event> allEvent = new HashSet<>();
            LocalDate day = LocalDate.of(oneYear.getKey(), 1, 1);
            while (n < oneYear.getValue().length()) {
                char element = oneYear.getValue().charAt(n);
                if (element != '0' && element != '4') {
                    EventDTO newEvent = new EventDTO(getNameTypeColorDay(element).get(0), getNameTypeColorDay(element).get(1), LocalDateTime.of(day.getYear(), day.getMonth(), day.getDayOfMonth(), 0, 0, 0, 1), LocalDateTime.of(day.getYear(), day.getMonth(), day.getDayOfMonth(), 23, 59, 59, 0), Integer.parseInt(getNameTypeColorDay(element).get(2)), List.of(1));
                    allEvent.add(eventService.eventDtoToEvent(newEvent));
                }
                day = day.plusDays(1);
                n++;
            }
            eventService.saveAll(allEvent);
        }
    }

    public List<String> getNameTypeColorDay(char chA) throws RuntimeException {
        if (chA == '1') {
            List<String> nameTypeColorDay = new ArrayList<>();
            nameTypeColorDay.add("Non-workingDay");
            nameTypeColorDay.add("239,239,239");
            nameTypeColorDay.add("1");
            return nameTypeColorDay;
        } else if (chA == '2') {
            List<String> nameTypeColorDay = new ArrayList<>();
            nameTypeColorDay.add("Half-holiday");
            nameTypeColorDay.add("***");
            nameTypeColorDay.add("2");
            return nameTypeColorDay;
        } else {
            throw new RuntimeException("Неизвестный тип дня");
        }
    }


}


