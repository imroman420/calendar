package com.calendarEFKO.services;

import com.calendarEFKO.exception.NotFoundObjectException;
import com.calendarEFKO.models.EventType;
import com.calendarEFKO.repositories.EventTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class EventTypeService {
    private final EventTypeRepository eventTypeRepository;

    @Autowired
    public EventTypeService(EventTypeRepository eventTypeRepository) {
        this.eventTypeRepository = eventTypeRepository;
    }

    @Transactional
    public EventType findOne(int id) throws NotFoundObjectException {
        return eventTypeRepository.findById(id).orElseThrow(() -> new NotFoundObjectException("Event Type с данным ID не найден"));
    }

    public List<EventType> findAll() {
        return eventTypeRepository.findAll();
    }

    @Transactional
    public EventType save(EventType eventType) {
        eventType.setDtCreation(LocalDateTime.now());
        return eventTypeRepository.save(eventType);
    }

    @Transactional
    public EventType update(int id, EventType eventType) {
        eventTypeRepository.findById(id).orElseThrow(() -> new NotFoundObjectException("Event Type с данным ID не найден"));
        eventType.setDtUpdate(LocalDateTime.now());
        return eventTypeRepository.save(eventType);
    }

    @Transactional
    public EventType delete(int id) {
        EventType eventType = eventTypeRepository.findById(id).orElseThrow(() -> new NotFoundObjectException("Event Type с данным ID не найден"));
        eventType.setDtDeletion(LocalDateTime.now());
        return eventTypeRepository.save(eventType);
    }


}
