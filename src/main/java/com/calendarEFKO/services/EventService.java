package com.calendarEFKO.services;

import com.calendarEFKO.dto.EventDTO;
import com.calendarEFKO.exception.NotFoundObjectException;
import com.calendarEFKO.models.Calendar;
import com.calendarEFKO.models.Event;
import com.calendarEFKO.repositories.EventRepository;
import com.calendarEFKO.specifications.SearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.calendarEFKO.specifications.EventSpecification.buildEventSpecification;


@Service
@Transactional(readOnly = true)
public class EventService {

    private final EventRepository eventRepository;

    private final CalendarService calendarService;

    private final EventTypeService eventTypeService;

    @Autowired
    public EventService(EventRepository eventRepository, CalendarService calendarService, EventTypeService eventTypeService) {
        this.eventRepository = eventRepository;
        this.calendarService = calendarService;
        this.eventTypeService = eventTypeService;
    }

    public Event findOne(int id) {
        return eventRepository.findById(id).orElseThrow(() -> new NotFoundObjectException("Event с данным ID не найден"));
    }

    public List<EventDTO> findAll(String name, Integer calendarId, Integer eventTypeId, LocalDateTime dtStart, LocalDateTime dtEnd) {
        List<SearchCriteria> params = new ArrayList<>();
        if (name != null) {
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setKey("name");
            searchCriteria.setOperation(":");
            searchCriteria.setValue(name);
            params.add(searchCriteria);
        }
        if (calendarId != null) {
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setKey("calendars");
            searchCriteria.setOperation(":");
            searchCriteria.setValue(calendarId);
            params.add(searchCriteria);
        }
        if (eventTypeId != null) {
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setKey("eventType");
            searchCriteria.setOperation(":");
            searchCriteria.setValue(eventTypeService.findOne(eventTypeId));
            params.add(searchCriteria);
        }
        if (dtStart != null) {
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setKey("dtStart");
            searchCriteria.setOperation(">");
            searchCriteria.setLocalDateTime(dtStart);
            params.add(searchCriteria);
        }
        if (dtEnd != null) {
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setKey("dtEnd");
            searchCriteria.setOperation("<");
            searchCriteria.setLocalDateTime(dtEnd);
            params.add(searchCriteria);
        }
        if ((buildEventSpecification(params)) != null) {
            return eventListToEventDtoList(eventRepository.findAll(buildEventSpecification(params)));
        } else {
            return eventListToEventDtoList(eventRepository.findAll());
        }

    }

    public List<Event> findWeekdaysParams(LocalDate dtStart, LocalDate dtEnd) {
        return eventRepository.findEventsProductionCalendar(1, dtStart.atTime(0, 0, 0, 0), dtEnd.atTime(0, 0, 0, 1)).stream().toList();
    }

    @Transactional
    public Event save(EventDTO eventDto) throws NotFoundObjectException {
        return eventRepository.save(eventDtoToEvent(eventDto));
    }

    @Transactional
    public void saveAll(Set<Event> event) {
        eventRepository.saveAll(event);
    }


    @Transactional
    public Event updateEvent(int id, EventDTO updateEventDto) {
        Event event = eventRepository.findById(id).orElseThrow(() -> new NotFoundObjectException("Event с данным ID не найден"));
        event.setName(updateEventDto.getName());
        event.setEventType(eventTypeService.findOne(updateEventDto.getEventTypeId()));
        event.setColor(updateEventDto.getColor());
        event.setMeta(updateEventDto.getMeta());
        event.setDtStart(updateEventDto.getDtStart());
        event.setDtEnd(updateEventDto.getDtEnd());
        event.setDtUpdate(LocalDateTime.now());
        return eventRepository.save(event);
    }

    @Transactional
    public Event deleteEvent(int id) {
        Event event = eventRepository.findById(id).orElseThrow(() -> new NotFoundObjectException("Event с данным ID не найден"));
        event.setDtDeletion(LocalDateTime.now());
        return eventRepository.save(event);
    }

    @Transactional
    public Event eventDtoToEvent(EventDTO eventDto) throws NotFoundObjectException {
        Event event = new Event();
        event.setName(eventDto.getName());
        event.setColor(eventDto.getColor());
        event.setMeta(eventDto.getMeta());
        event.setDtStart(eventDto.getDtStart());
        event.setDtEnd(eventDto.getDtEnd());
        if (eventDto.getEventTypeId() != null) {
            event.setEventType(eventTypeService.findOne(eventDto.getEventTypeId()));
        }
        if (eventDto.getCalendarId() != null) {
            List<Integer> calendarsIdS = eventDto.getCalendarId();
            Set<Calendar> CalendarSet = new HashSet<>();
            for (Integer calendarsId : calendarsIdS) {
                CalendarSet.add(calendarService.findOne(calendarsId));
            }
            event.setCalendars(CalendarSet);
        }
        event.setDtCreation(LocalDateTime.now());
        return event;
    }

    @Transactional
    public EventDTO eventToEventDto(Event event) {
        EventDTO eventDto = new EventDTO();
        eventDto.setId(event.getId());
        eventDto.setName(event.getName());
        eventDto.setColor(event.getColor());
        eventDto.setMeta(event.getMeta());
        eventDto.setDtStart(event.getDtStart());
        eventDto.setDtEnd(event.getDtEnd());
        if (event.getEventType() != null) {
            eventDto.setEventTypeId(event.getEventType().getId());
        }
        if (event.getCalendars() != null) {
            List<Integer> CalendarIdS = new ArrayList<>();
            for (Calendar calendar : event.getCalendars()) {
                CalendarIdS.add(calendar.getId());
            }
            eventDto.setCalendarId(CalendarIdS);
        }
        return eventDto;
    }

    @Transactional
    public List<Event> eventDtoListToEventList(List<EventDTO> eventDtoList) throws NotFoundObjectException {
        List<Event> eventList = new ArrayList<>();
        for (EventDTO eventDto : eventDtoList) {
            eventList.add(eventDtoToEvent(eventDto));
        }
        return eventList;
    }

    @Transactional
    public List<EventDTO> eventListToEventDtoList(List<Event> eventList) {
        List<EventDTO> eventDtoList = new ArrayList<>();
        for (Event event : eventList) {
            eventDtoList.add(eventToEventDto(event));
        }
        return eventDtoList;
    }
}
