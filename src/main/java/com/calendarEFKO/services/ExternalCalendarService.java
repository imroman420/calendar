package com.calendarEFKO.services;

import com.calendarEFKO.exception.ValidException;
import com.calendarEFKO.models.Calendar;
import com.calendarEFKO.models.Event;
import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.ComponentList;
import net.fortuna.ical4j.model.component.VEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.io.StringReader;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Service
public class ExternalCalendarService {

    private final CalendarService calendarService;

    private final UserService userService;

    private final EventTypeService eventTypeService;
    private final EventService eventService;


    @Autowired
    ExternalCalendarService(CalendarService calendarService, UserService userService, EventTypeService eventTypeService, EventService eventService) {
        this.calendarService = calendarService;
        this.userService = userService;
        this.eventTypeService = eventTypeService;
        this.eventService = eventService;

    }


    @Transactional
    public Calendar addExternalCalendar(Authentication authentication, String name, String color, String url) {
        Calendar calendar = new Calendar();
        calendar.setName(name);
        calendar.setColor(color);
        //calendar.setOwner(userService.findOne(((JwtAuthenticationToken) authentication).getToken().getClaim("sid")));
        calendar.setOwner(userService.findOne("198419841984198419841984198419841984"));
        calendarService.save(calendar);
        eventService.saveAll(transformExternalCalendar(getExternalCalendar(url), calendar));
        return calendar;
    }

    //добавить эксепшены на 400 и 500
    public String getExternalCalendar(String url) throws RuntimeException {
        try {
            RestTemplate restTemplate = new RestTemplate();

            return restTemplate.getForEntity(url.replaceAll("%40", "@"), String.class).getBody();
        } catch (RuntimeException e) {
            return e.getMessage();
        }
    }

    @Transactional
    public Set<Event> transformExternalCalendar(String caldav, Calendar calendar) {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
        DateFormat dateFormatLocalDate = new SimpleDateFormat("yyyyMMdd");
        try {
            CalendarBuilder builder = new CalendarBuilder();
            StringReader stringReader = new StringReader(caldav);
            net.fortuna.ical4j.model.Calendar calendarCaldav = builder.build(stringReader);
            Set<Event> eventSet = new HashSet<>();
            ComponentList listEventCalDav = calendarCaldav.getComponents(net.fortuna.ical4j.model.Component.VEVENT);
            for (Object elemCalDav : listEventCalDav) {
                VEvent vEvent = (VEvent) elemCalDav;
                Event event = new Event();
                event.setName(vEvent.getSummary().getValue());
                event.setEventType(eventTypeService.findOne(4));
                event.setColor(calendar.getColor());
                event.setCalendars(Set.of(calendar));
                event.setMeta(vEvent.getDescription().getValue());
                if (vEvent.getStartDate().getValue().length() + vEvent.getEndDate().getValue().length() > 29) {
                    event.setDtStart(convertToLocalDateTimeViaInstant(dateFormat.parse(vEvent.getStartDate().getValue())));
                    event.setDtEnd(convertToLocalDateTimeViaInstant(dateFormat.parse(vEvent.getEndDate().getValue())));
                } else {
                    event.setDtStart(convertToLocalDateTimeViaInstantAtTime(dateFormatLocalDate.parse(vEvent.getStartDate().getValue())));
                    event.setDtEnd(convertToLocalDateTimeViaInstantAtTime(dateFormatLocalDate.parse(vEvent.getEndDate().getValue())));
                }
                event.setDtCreation(LocalDateTime.now());
                eventSet.add(event);
            }
            return eventSet;
        } catch (Exception e) {
            System.err.println();
        }
        throw new ValidException("Ошибка валидации и парсинга внешнего календаря", System.currentTimeMillis());
    }

    public LocalDateTime convertToLocalDateTimeViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

    //поменять местами, сначала часы, потом преобразованиев в локдаттайм
    public LocalDateTime convertToLocalDateTimeViaInstantAtTime(Date dateToConvert) {
        LocalDate localDate = dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        return localDate.atTime(0, 0, 0, 0);
    }
}
