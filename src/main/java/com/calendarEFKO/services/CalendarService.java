package com.calendarEFKO.services;

import com.calendarEFKO.exception.NotFoundObjectException;
import com.calendarEFKO.models.Calendar;
import com.calendarEFKO.repositories.CalendarRepository;
import com.calendarEFKO.repositories.UserRepository;
import com.calendarEFKO.specifications.SearchCriteria;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.calendarEFKO.specifications.CalendarSpecification.buildCalendarSpecification;

/**
 * Сервисный слой calendar, отвечает за бизнес логику и взаимодействие репозитория и контроллера
 */
@Service
@Transactional(readOnly = true)
public class CalendarService {
    private final CalendarRepository calendarRepository;

    private final UserRepository userRepository;

    @Autowired
    public CalendarService(CalendarRepository calendarRepository, UserRepository userRepository) {
        this.calendarRepository = calendarRepository;
        this.userRepository = userRepository;
    }

    public Calendar findOne(int id) throws NotFoundObjectException {
        return calendarRepository.findById(id).orElseThrow(() -> new NotFoundObjectException("Calendar с данным ID не найден"));
    }

    public List<Calendar> findAll(String name, String ownerId) {
        List<SearchCriteria> params = new ArrayList<SearchCriteria>();
        if (name != null) {
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setKey("name");
            searchCriteria.setOperation(":");
            searchCriteria.setValue(name);
            params.add(searchCriteria);
        }
        if (ownerId != null) {
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setKey("owner");
            searchCriteria.setOperation(":");
            searchCriteria.setValue(userRepository.findById(ownerId));
            params.add(searchCriteria);
        }
        if ((buildCalendarSpecification(params)) != null) {
            return calendarRepository.findAll(buildCalendarSpecification(params));
        } else {
            return calendarRepository.findAll();
        }
    }

    @Transactional
    public Calendar save(Calendar calendar) {
        calendar.setDtCreation(LocalDateTime.now());
        return calendarRepository.save(calendar);
    }


    @Transactional
    public Calendar update(Calendar updateCalendar, int id) {
        Calendar calendar = calendarRepository.findById(id).orElseThrow(() -> new NotFoundObjectException("Calendar с данным ID не найден"));
        BeanUtils.copyProperties(calendar, updateCalendar);
        calendar.setDtUpdate(LocalDateTime.now());
        return calendarRepository.save(calendar);
    }

    @Transactional
    public Calendar delete(int id) {
        Calendar calendar = calendarRepository.findById(id).orElseThrow(() -> new NotFoundObjectException("Calendar с данным ID не найден"));
        calendar.setDtDeletion(LocalDateTime.now());
        return calendar;
    }
}

