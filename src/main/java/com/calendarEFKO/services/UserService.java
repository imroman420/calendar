package com.calendarEFKO.services;

import com.calendarEFKO.exception.NotFoundObjectException;
import com.calendarEFKO.models.Calendar;
import com.calendarEFKO.models.User;
import com.calendarEFKO.repositories.UserRepository;
import com.calendarEFKO.specifications.SearchCriteria;
import com.calendarEFKO.specifications.UserSpecification;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.calendarEFKO.specifications.UserSpecification.buildUserSpecification;

@Service
@Transactional(readOnly = true)
public class UserService {

    private final UserRepository userRepository;
    private final CalendarService calendarService;

    @Autowired
    public UserService(UserRepository userRepository, CalendarService calendarService) {
        this.calendarService = calendarService;
        this.userRepository = userRepository;
    }

    public User findMe(Authentication authentication) {
        return userRepository.findById((((JwtAuthenticationToken) authentication).getToken().getClaim("sid")).toString());
    }

    public User findOne(String id) throws NotFoundObjectException {
        if (userRepository.findById(id) == null) {
            throw new NotFoundObjectException("User с данным ID не найден");
        } else return userRepository.findById(id);
    }

    public List<User> findAll(String id, String login, String email, String firstName, String middleName, String lastName, String company, String department, String position) {
        List<SearchCriteria> params = new ArrayList<SearchCriteria>();
        if(id !=null){
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setKey("id");
            searchCriteria.setOperation(":");
            searchCriteria.setValue(id);
            params.add(searchCriteria);
        }

        if(login !=null){
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setKey("login");
            searchCriteria.setOperation(":");
            searchCriteria.setValue(login);
            params.add(searchCriteria);
        }

        if(email !=null){
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setKey("email");
            searchCriteria.setOperation(":");
            searchCriteria.setValue(email);
            params.add(searchCriteria);
        }

        if(firstName !=null){
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setKey("firstName");
            searchCriteria.setOperation(":");
            searchCriteria.setValue(firstName);
            params.add(searchCriteria);
        }

        if(middleName !=null){
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setKey("middleName");
            searchCriteria.setOperation(":");
            searchCriteria.setValue(middleName);
            params.add(searchCriteria);
        }

        if(lastName !=null){
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setKey("lastName");
            searchCriteria.setOperation(":");
            searchCriteria.setValue(lastName);
            params.add(searchCriteria);
        }

        if(company !=null){
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setKey("company");
            searchCriteria.setOperation(":");
            searchCriteria.setValue(company);
            params.add(searchCriteria);
        }

        if(department !=null){
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setKey("department");
            searchCriteria.setOperation(":");
            searchCriteria.setValue(department);
            params.add(searchCriteria);
        }

        if(position !=null){
            SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria.setKey("position");
            searchCriteria.setOperation(":");
            searchCriteria.setValue(position);
            params.add(searchCriteria);
        }
        if((buildUserSpecification(params))!=null) {
            return userRepository.findAll(buildUserSpecification(params));
        }
        else{
            return userRepository.findAll();
        }
    }

    public User saveMe(Authentication authentication) {
        User user = new User();
        user.setId(((JwtAuthenticationToken) authentication).getToken().getClaim("sid"));
        user.setLogin(((JwtAuthenticationToken) authentication).getToken().getClaim("login"));
        user.setEmail(((JwtAuthenticationToken) authentication).getToken().getClaim("email"));
        user.setFirstName(((JwtAuthenticationToken) authentication).getToken().getClaim("firstName"));
        user.setMiddleName(((JwtAuthenticationToken) authentication).getToken().getClaim("middleName"));
        user.setLastName(((JwtAuthenticationToken) authentication).getToken().getClaim("lastName"));
        user.setCompany(((JwtAuthenticationToken) authentication).getToken().getClaim("company"));
        user.setDepartment(((JwtAuthenticationToken) authentication).getToken().getClaim("department"));
        user.setPosition(((JwtAuthenticationToken) authentication).getToken().getClaim("position"));
        user.setAvatar(((JwtAuthenticationToken) authentication).getToken().getClaim("avatar"));
        user.setTimezone(((JwtAuthenticationToken) authentication).getToken().getClaim("timezone"));
        save(user);
        Calendar calendar = new Calendar();
        calendar.setName(User.convertName(((JwtAuthenticationToken) authentication).getToken().getClaim("lastName"), ((JwtAuthenticationToken) authentication).getToken().getClaim("firstName"), ((JwtAuthenticationToken) authentication).getToken().getClaim("middleName")));
        calendar.setOwner(user);
        calendar.setColor("#3b8ad4");
        calendarService.save(calendar);
        return user;
    }

    @Transactional
    public User save(User createUser) {
        createUser.setDtCreation(LocalDateTime.now());
        return userRepository.save(createUser);
    }

    @Transactional
    public User update(int id, User updateUser) throws NotFoundObjectException {
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundObjectException("User с данным ID не найден"));
        BeanUtils.copyProperties(user, updateUser);
        user.setDtUpdate(LocalDateTime.now());
        return userRepository.save(user);
    }

    @Transactional
    public User delete(int id) throws NotFoundObjectException {
        User user = userRepository.findById(id).orElseThrow(() -> new NotFoundObjectException("User с данным ID не найден"));
        user.setDtDeletion(LocalDateTime.now());
        return userRepository.save(user);
    }

}
