package com.calendarEFKO;

//import com.calendarEFKO.services.ExternalCalendarService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import net.fortuna.ical4j.data.ParserException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;


@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "CalendarEFKO API", version = "1.0", description = "Calendars Information"))
//@EnableScheduling
public class CalendarEFKOApplication {

    public static void main(String[] args) {
        SpringApplication.run(CalendarEFKOApplication.class, args);

    }

}