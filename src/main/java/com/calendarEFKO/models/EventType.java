package com.calendarEFKO.models;

import com.calendarEFKO.view.Views;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "event_type")
public class EventType {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private int id;

    @Column(name = "name")
    @NotBlank(message = "Name must not be empty")
    @Size(min = 2, max = 60, message = "The number of characters does not meet the requirements (2-60)")
    @JsonView(Views.Public.class)
    private String name;

    @OneToMany(mappedBy = "eventType", fetch = FetchType.EAGER)
    @JsonIgnore
    private List<Event> event;

    @Column(name = "dt_creation")
    @DateTimeFormat(fallbackPatterns = {"yyyy.MM.dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss"})
    @JsonView(Views.Private.class)
    private LocalDateTime dtCreation;

    @Column(name = "dt_update")
    @DateTimeFormat(fallbackPatterns = {"yyyy.MM.dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss"})
    @JsonView(Views.Private.class)
    private LocalDateTime dtUpdate;

    @Column(name = "dt_deletion")
    @DateTimeFormat(fallbackPatterns = {"yyyy.MM.dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss"})
    @JsonView(Views.Private.class)
    private LocalDateTime dtDeletion;

    public EventType() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getDtCreation() {
        return dtCreation;
    }

    public void setDtCreation(LocalDateTime dtCreation) {
        this.dtCreation = dtCreation;
    }

    public LocalDateTime getDtUpdate() {
        return dtUpdate;
    }

    public void setDtUpdate(LocalDateTime dtUpdate) {
        this.dtUpdate = dtUpdate;
    }

    public LocalDateTime getDtDeletion() {
        return dtDeletion;
    }

    public void setDtDeletion(LocalDateTime dtDeletion) {
        this.dtDeletion = dtDeletion;
    }
}
