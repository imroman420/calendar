package com.calendarEFKO.models;

import com.calendarEFKO.view.Views;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Set;

//@Data
//@Builder
//@NoArgsConstructor
//@AllArgsConstructor
@Entity
@Table(name = "event")
@Schema(description = "Информация о пользователе")
public class Event {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private int id;

    @Column(name = "name")
    @NotBlank(message = "Name must not be empty")
    @Size(min = 2, max = 60, message = "The number of characters does not meet the requirements (2-60)")
    @JsonView(Views.Public.class)
    private String name;

    @Column(name = "color")
    @NotBlank(message = "Name must not be empty")
    @Size(min = 2, max = 60, message = "The number of characters does not meet the requirements (2-60)")
    @JsonView(Views.Public.class)
    private String color;


    @Column(name = "meta")
    @Size(max = 500, message = "The number of characters does not meet the requirements (max500)")
    @JsonView(Views.Public.class)
    private String meta;

    @Column(name = "dt_start")
    @DateTimeFormat(fallbackPatterns = {"yyyy.MM.dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss"})
    @JsonView(Views.Public.class)
    private LocalDateTime dtStart;

    @Column(name = "dt_end")
    @DateTimeFormat(fallbackPatterns = {"yyyy.MM.dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss"})
    @JsonView(Views.Public.class)
    private LocalDateTime dtEnd;

    @ManyToOne
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @JoinColumn(name = "event_type_id", referencedColumnName = "id")
    private EventType eventType;

    @ManyToMany
    @JoinTable(name = "event_link_calendar", joinColumns = @JoinColumn(name = "event_id"), inverseJoinColumns = @JoinColumn(name = "calendar_id"))
    @JsonIgnore
    private Set<Calendar> calendars;

    @Column(name = "dt_creation")
    @DateTimeFormat(fallbackPatterns = {"yyyy.MM.dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss"})
    @JsonView(Views.Private.class)
    private LocalDateTime dtCreation;

    @Column(name = "dt_update")
    @DateTimeFormat(fallbackPatterns = {"yyyy.MM.dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss"})
    @JsonView(Views.Private.class)
    private LocalDateTime dtUpdate;

    @Column(name = "dt_deletion")
    @DateTimeFormat(fallbackPatterns = {"yyyy.MM.dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss"})
    @JsonView(Views.Private.class)
    private LocalDateTime dtDeletion;

    public Event() {
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getDtStart() {
        return dtStart;
    }

    public void setDtStart(LocalDateTime dtStart) {
        this.dtStart = dtStart;
    }

    public LocalDateTime getDtEnd() {
        return dtEnd;
    }

    public void setDtEnd(LocalDateTime dtEnd) {
        this.dtEnd = dtEnd;
    }

    public Set<Calendar> getCalendars() {
        return calendars;
    }

    public void setCalendars(Set<Calendar> calendars) {
        this.calendars = calendars;
    }

    public LocalDateTime getDtCreation() {
        return dtCreation;
    }

    public void setDtCreation(LocalDateTime dtCreation) {
        this.dtCreation = dtCreation;
    }

    public LocalDateTime getDtUpdate() {
        return dtUpdate;
    }

    public void setDtUpdate(LocalDateTime dtUpdate) {
        this.dtUpdate = dtUpdate;
    }

    public LocalDateTime getDtDeletion() {
        return dtDeletion;
    }

    public void setDtDeletion(LocalDateTime dtDeletion) {
        this.dtDeletion = dtDeletion;
    }

}



