package com.calendarEFKO.models;

import com.calendarEFKO.view.Views;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;
import org.hibernate.annotations.Cascade;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "users")
@Schema(description = "Модель пользователя")
public class User {

    @Id
    @Schema(description = "ID пользователя")
    @Column(name = "id")
    @NotBlank(message = "ID must not be empty")
    @Size(min = 36, max = 36, message = "The number of characters does not meet the requirements (36)")
    @JsonView(Views.Public.class)
    private String id;
    @Schema(description = "Логин пользователя")
    @Column(name = "login")
    @NotBlank(message = "Login must not be empty")
    @Size(min = 2, max = 60, message = "The number of characters does not meet the requirements (2-60)")
    @JsonView(Views.Public.class)
    private String login;
    @Schema(description = "Почта пользователя")
    @Email(message = "Email is not valid", regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[efko]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$")
    @Column(name = "email")
    @NotBlank(message = "Name must not be empty")
    @Size(min = 2, max = 60, message = "The number of characters does not meet the requirements (2-60)")
    @JsonView(Views.Public.class)
    private String email;
    @Schema(description = "Список Календарей пользователя")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    @OneToMany(mappedBy = "owner")
    @JsonIgnore
    private List<Calendar> calendar;
    @Schema(description = "Имя пользователя")
    @Column(name = "first_name")
    @NotBlank(message = "Name must not be empty")
    @Size(min = 2, max = 20, message = "The number of characters does not meet the requirements (2-20)")
    @JsonView(Views.Public.class)
    private String firstName;

    @Schema(description = "Отчество пользователя")
    @Column(name = "middle_name")
    @Size(min = 2, max = 20, message = "The number of characters does not meet the requirements (2-20)")
    @JsonView(Views.Public.class)
    private String middleName;

    @Schema(description = "Фамилия пользователя")
    @Column(name = "last_name")
    @NotBlank(message = "Name must not be empty")
    @Size(min = 2, max = 30, message = "The number of characters does not meet the requirements (2-30)")
    @JsonView(Views.Public.class)
    private String lastName;

    @Schema(description = "Компания пользователя")
    @Column(name = "company")
    @Size(min = 2, max = 30, message = "The number of characters does not meet the requirements (2-50)")
    @JsonView(Views.Public.class)
    private String company;

    @Schema(description = "Подразделение пользователя")
    @Column(name = "department")
    @Size(min = 2, max = 50, message = "The number of characters does not meet the requirements (2-50)")
    @JsonView(Views.Public.class)
    private String department;

    @Schema(description = "СРП пользователя")
    @Column(name = "position")
    @Size(min = 2, max = 50, message = "The number of characters does not meet the requirements (2-50)")
    @JsonView(Views.Public.class)
    private String position;

    @Schema(description = "Аватар пользователя")
    @Column(name = "avatar")
    @Size(min = 2, max = 50, message = "The number of characters does not meet the requirements (2-50)")
    @JsonView(Views.Public.class)
    private String avatar;


    @Schema(description = "Timezone")
    @Column(name = "timezone")
    @Size(min = 2, max = 60, message = "The number of characters does not meet the requirements (2-30)")
    @JsonView(Views.Public.class)
    private String timezone;

    @Hidden
    @Column(name = "dt_creation")
    @DateTimeFormat(fallbackPatterns = {"yyyy.MM.dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss"})
    @JsonView(Views.Private.class)
    private LocalDateTime dtCreation;

    @Hidden
    @Column(name = "dt_update")
    @DateTimeFormat(fallbackPatterns = {"yyyy.MM.dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss"})
    @JsonView(Views.Private.class)
    private LocalDateTime dtUpdate;

    @Hidden
    @Column(name = "dt_deletion")
    @DateTimeFormat(fallbackPatterns = {"yyyy.MM.dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss"})
    @JsonView(Views.Private.class)
    private LocalDateTime dtDeletion;

    public User() {
    }

    public User(String id, String login, String email, List<Calendar> calendar, String firstName, String middleName, String lastName, String company, String department, String position, String avatar, String timezone, LocalDateTime dtCreation, LocalDateTime dtUpdate, LocalDateTime dtDeletion) {
        this.id = id;
        this.login = login;
        this.email = email;
        this.calendar = calendar;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.company = company;
        this.department = department;
        this.position = position;
        this.avatar = avatar;
        this.timezone = timezone;
        this.dtCreation = dtCreation;
        this.dtUpdate = dtUpdate;
        this.dtDeletion = dtDeletion;
    }

    public static String convertName(String lastName, String firstName, String middleName) {
        return lastName + " " + lastName + " " + lastName;
    }

    public LocalDateTime getDtCreation() {
        return dtCreation;
    }

    public void setDtCreation(LocalDateTime dtCreation) {
        this.dtCreation = dtCreation;
    }

    public LocalDateTime getDtUpdate() {
        return dtUpdate;
    }

    public void setDtUpdate(LocalDateTime dtUpdate) {
        this.dtUpdate = dtUpdate;
    }

    public LocalDateTime getDtDeletion() {
        return dtDeletion;
    }

    public void setDtDeletion(LocalDateTime dtDeletion) {
        this.dtDeletion = dtDeletion;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCalendar(List<Calendar> calendar) {
        this.calendar = calendar;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}


