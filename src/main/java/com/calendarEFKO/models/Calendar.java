package com.calendarEFKO.models;

import com.calendarEFKO.view.Views;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name = "calendar")
@Schema(description = "Модель календаря")
public class Calendar {

    @Hidden
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonView(Views.Public.class)
    private int id;

    @Column(name = "name")
    @NotBlank(message = "Name must not be empty")
    @Size(min = 2, max = 60, message = "The number of characters does not meet the requirements (2-60)")
    @JsonView(Views.Public.class)
    private String name;

    @Column(name = "color")
    @NotBlank(message = "Name must not be empty")
    @Size(min = 2, max = 60, message = "The number of characters does not meet the requirements (2-60)")
    @JsonView(Views.Public.class)
    private String color;

    @ManyToOne
    @JoinColumn(name = "owner_id", referencedColumnName = "id", insertable = false, updatable = false)
    @JsonIgnore
    private User owner;

    @ManyToMany(mappedBy = "calendars")
    @JsonIgnore
    private Set<Event> events;
    @Hidden
    @Column(name = "dt_creation")
    @DateTimeFormat(fallbackPatterns = {"yyyy.MM.dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss"})
    @JsonView(Views.Private.class)
    private LocalDateTime dtCreation;
    @Hidden
    @Column(name = "dt_update")
    @DateTimeFormat(fallbackPatterns = {"yyyy.MM.dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss"})
    @JsonView(Views.Private.class)
    private LocalDateTime dtUpdate;
    @Hidden
    @Column(name = "dt_deletion")
    @DateTimeFormat(fallbackPatterns = {"yyyy.MM.dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss"})
    @JsonView(Views.Private.class)
    private LocalDateTime dtDeletion;

    public Calendar() {
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public LocalDateTime getDtCreation() {
        return dtCreation;
    }

    public void setDtCreation(LocalDateTime dtCreation) {
        this.dtCreation = dtCreation;
    }

    public LocalDateTime getDtUpdate() {
        return dtUpdate;
    }

    public void setDtUpdate(LocalDateTime dtUpdate) {
        this.dtUpdate = dtUpdate;
    }

    public LocalDateTime getDtDeletion() {
        return dtDeletion;
    }

    public void setDtDeletion(LocalDateTime dtDeletion) {
        this.dtDeletion = dtDeletion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @JsonIgnore
    public User getOwner() {
        return owner;
    }

    @JsonIgnore
    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Event> getEvents() {
        return events;
    }

    public void setEvents(Set<Event> events) {
        this.events = events;
    }
}