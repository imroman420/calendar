package com.calendarEFKO.exception;

public class NotCreatedObjectException extends RuntimeException {
    public NotCreatedObjectException(String msg) {
        super(msg);
    }
}
