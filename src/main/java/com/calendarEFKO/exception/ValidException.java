package com.calendarEFKO.exception;

public class ValidException extends RuntimeException {
    public ValidException(String msg, long l) {
        super(msg);
    }
}
