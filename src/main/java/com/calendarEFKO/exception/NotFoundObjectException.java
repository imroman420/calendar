package com.calendarEFKO.exception;

public class NotFoundObjectException extends RuntimeException {

    public NotFoundObjectException(String msg) {
        super(msg);

    }
}
