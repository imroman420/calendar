package com.calendarEFKO.dto;

import com.calendarEFKO.models.Event;

public record ResultEvent<T>(Event data) {
}
