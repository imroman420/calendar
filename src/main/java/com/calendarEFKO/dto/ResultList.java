package com.calendarEFKO.dto;

import com.calendarEFKO.view.Views;
import com.fasterxml.jackson.annotation.JsonView;

import java.util.List;

public class ResultList<T> {
    @JsonView(Views.Public.class)
    private List<T> data;

    public ResultList(List<T> data) {
        this.data = data;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

}


