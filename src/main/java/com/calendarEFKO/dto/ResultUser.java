package com.calendarEFKO.dto;

import com.calendarEFKO.models.User;

public record ResultOrders<T>(Orders data) {
}