package com.calendarEFKO.dto;

import com.calendarEFKO.models.Event;
import com.calendarEFKO.services.EventService;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Schema(description = "Модель ивента")
public class EventDTO {
    private Integer Id;
    @NotBlank(message = "Name must not be empty")
    @Size(min = 2, max = 60, message = "The number of characters does not meet the requirements (2-60)")
    private String name;

    @NotBlank(message = "Name must not be empty")
    @Size(min = 2, max = 60, message = "The number of characters does not meet the requirements (2-60)")
    private String color;


    @Size(max = 500, message = "The number of characters does not meet the requirements (max500)")
    private String meta;

    @DateTimeFormat(fallbackPatterns = {"yyyy.MM.dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss"})
    private LocalDateTime dtStart;

    @DateTimeFormat(fallbackPatterns = {"yyyy.MM.dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss"})
    private LocalDateTime dtEnd;
    private Integer eventTypeId;
    private List<Integer> calendarId;

    public EventDTO(Integer Id, String name, String color, String meta, LocalDateTime dtStart, LocalDateTime dtEnd, Integer eventTypeId, List<Integer> calendarId) {
        this.name = name;
        this.color = color;
        this.meta = meta;
        this.dtStart = dtStart;
        this.dtEnd = dtEnd;
        this.eventTypeId = eventTypeId;
        this.calendarId = calendarId;
        this.Id = Id;
    }

    public EventDTO(String name, String color, LocalDateTime dtStart, LocalDateTime dtEnd, Integer eventTypeId, List<Integer> calendarId) {
        this.name = name;
        this.color = color;
        this.dtStart = dtStart;
        this.dtEnd = dtEnd;
        this.eventTypeId = eventTypeId;
        this.calendarId = calendarId;
    }


    public  EventDTO(){
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }

    public LocalDateTime getDtStart() {
        return dtStart;
    }

    public void setDtStart(LocalDateTime dtStart) {
        this.dtStart = dtStart;
    }

    public LocalDateTime getDtEnd() {
        return dtEnd;
    }

    public void setDtEnd(LocalDateTime dtEnd) {
        this.dtEnd = dtEnd;
    }

    public Integer getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(Integer eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public List<Integer> getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(List<Integer> calendarId) {
        this.calendarId = calendarId;
    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer Id) {
        this.Id = Id;
    }
}
