/*
package com.calendarEFKO.dto;

import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Schema(description = "Модель календаря")
public class CalendarDTO {

    @NotEmpty(message = "Name must not be empty")
    @Size(min = 5, max = 60, message = "The number of characters does not meet the requirements (5-60)")
    private String name;

    @NotNull
    private int ownerId;

    public CalendarDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }
}
*/
