package com.calendarEFKO.dto;

public class CalDavDto {
    private String name;
    private String color;
    private String urlCalDav;

    public CalDavDto(String name, String color, String urlCalDav) {
        this.name = name;
        this.color = color;
        this.urlCalDav = urlCalDav;
    }

    public CalDavDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getUrlCalDav() {
        return urlCalDav;
    }

    public void setUrlCalDav(String urlCalDav) {
        this.urlCalDav = urlCalDav;
    }
}
