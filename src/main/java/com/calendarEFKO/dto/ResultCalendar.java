package com.calendarEFKO.dto;

import com.calendarEFKO.models.Calendar;

public record ResultCalendar<T>(Calendar data) {
}