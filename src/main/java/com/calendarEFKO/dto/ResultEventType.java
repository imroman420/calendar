package com.calendarEFKO.dto;

import com.calendarEFKO.models.EventType;

public record ResultEventType<T>(EventType data) {
}
