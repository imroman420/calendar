ALTER TABLE event_link_calendar
  ADD CONSTRAINT `FK_event_link_calendar_calendar` FOREIGN KEY (`calendar_id`) REFERENCES `calendar` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;