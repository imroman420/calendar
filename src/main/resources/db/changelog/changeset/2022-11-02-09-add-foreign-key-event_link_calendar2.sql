ALTER TABLE event_link_calendar
  ADD CONSTRAINT `FK_event_link_calendar_event` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;