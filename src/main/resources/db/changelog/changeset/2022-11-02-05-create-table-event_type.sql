CREATE TABLE IF NOT EXISTS `event_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL,
  `dt_creation` timestamp NULL DEFAULT NULL,
  `dt_update` timestamp NULL DEFAULT NULL,
  `dt_deletion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `KEY_name` (`name`),
  KEY `KEY_creation` (`dt_creation`),
  KEY `KEY_update` (`dt_update`),
  KEY `KEY_deletion` (`dt_deletion`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;