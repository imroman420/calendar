CREATE TABLE IF NOT EXISTS `event_link_calendar` (
  `event_id` int(10) unsigned NOT NULL,
  `calendar_id` int(10) unsigned NOT NULL,
  KEY `event_id` (`event_id`),
  KEY `calendar_id` (`calendar_id`),
  KEY `event_id_and_calendar_id` (`calendar_id`,`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;