CREATE TABLE IF NOT EXISTS `users` (
  `id` varchar(50) DEFAULT NULL,
  `login` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) NULL DEFAULT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `company` varchar(50) NULL DEFAULT NULL,
  `department` varchar(50) NULL DEFAULT NULL,
  `position` varchar(50) NULL DEFAULT NULL,
  `avatar` varchar(50) NULL DEFAULT NULL,
  `timezone` varchar(50) NULL DEFAULT NULL,
  `dt_creation` timestamp NULL DEFAULT NULL,
  `dt_update` timestamp NULL DEFAULT NULL,
  `dt_deletion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `in_id` (`id`) USING BTREE,
  KEY `in_login` (`login`) USING BTREE,
  KEY `in_first_name` (`first_name`) USING BTREE,
  KEY `in_middle_name` (`middle_name`) USING BTREE,
  KEY `in_lastName` (`last_name`) USING BTREE,
  KEY `in_email` (`email`) USING BTREE,
  KEY `in_company` (`company`) USING BTREE,
  KEY `in_department` (`department`) USING BTREE,
  KEY `in_avatar` (`avatar`) USING BTREE,
  KEY `in_position` (`position`) USING BTREE,
  KEY `in_timezone` (`timezone`) USING BTREE,
  KEY `KEY_deletion` (`dt_deletion`),
  KEY `KEY_update` (`dt_update`),
  KEY `KEY_creation` (`dt_creation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;