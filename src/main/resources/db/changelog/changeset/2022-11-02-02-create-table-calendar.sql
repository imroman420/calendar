CREATE TABLE IF NOT EXISTS `calendar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(300) NOT NULL,
  `owner_id` varchar(50) DEFAULT NULL,
  `color` varchar(50) NOT NULL,
  `dt_creation` timestamp NULL DEFAULT NULL,
  `dt_update` timestamp NULL DEFAULT NULL,
  `dt_deletion` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_calendar_users` (`owner_id`),
  KEY `IN_name` (`name`) USING BTREE,
  KEY `color` (`color`) USING BTREE,
  KEY `KEY_update` (`dt_update`),
  KEY `KEY_deletion` (`dt_deletion`),
  KEY `KEY_creation` (`dt_creation`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;