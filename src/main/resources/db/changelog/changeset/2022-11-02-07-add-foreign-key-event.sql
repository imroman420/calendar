ALTER TABLE event
  ADD CONSTRAINT `FK_event_event_type` FOREIGN KEY (`event_type_id`) REFERENCES `event_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;