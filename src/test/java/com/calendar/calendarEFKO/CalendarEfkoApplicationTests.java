package com.calendar.calendarEFKO;

import com.calendarEFKO.models.User;
import com.calendarEFKO.repositories.CalendarRepository;
import com.calendarEFKO.repositories.UserRepository;
import com.calendarEFKO.services.CalendarService;
import com.calendarEFKO.services.UserService;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CalendarEfkoApplicationTests {

    @Test
    void contextLoads() {
    }

    @org.junit.Test
    public void userService_getAllUser_success() {
        //Given
        List<User> users = List.of(new User("1984", "orwell1984", "orwell1984@gmail.com", null, "George", "ErikBlair", "Orwell", "bigbrother", "inquest", "inquisitor", null, null, LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now()), new User("1985", "orwell1985", "orwell1985@gmail.com", null, "George", "ErikBlair", "Orwell", "bigbrother", "inquest", "inquisitor", null, null, LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now()));

        UserRepository userRepositoryMock = mock(UserRepository.class);
        CalendarRepository calendarRepositoryMock = mock(CalendarRepository.class);

        when(userRepositoryMock.findAll()).thenReturn(users);
        when(calendarRepositoryMock.findAll()).thenReturn(null);

        CalendarService calendarServiceMock = new CalendarService(calendarRepositoryMock, userRepositoryMock);
        UserService userServiceMock = new UserService(userRepositoryMock, calendarServiceMock);

        //When
        List<User> userList = userServiceMock.findAll(null, null, null, null, null, null, null, null, null);

        //Then
        assertNotNull(userList);
        assertEquals(users, userList);
    }

    @org.junit.Test
    public void userService_getUserById_success() {
        //Given
        User user1 = new User("1985", "orwell1985", "orwell1985@gmail.com", null, "George", "ErikBlair", "Orwell", "bigbrother", "inquest", "inquisitor", null, null, LocalDateTime.now(), LocalDateTime.now(), LocalDateTime.now());

        UserRepository userRepositoryMock = mock(UserRepository.class);
        CalendarRepository calendarRepositoryMock = mock(CalendarRepository.class);

        when(userRepositoryMock.findById("1985")).thenReturn(user1);
        when(calendarRepositoryMock.findAll()).thenReturn(null);

        CalendarService calendarServiceMock = new CalendarService(calendarRepositoryMock, userRepositoryMock);
        UserService userServiceMock = new UserService(userRepositoryMock, calendarServiceMock);

        //When
        User user = userServiceMock.findOne("1985");

        //Then
        assertNotNull(user);
        assertEquals(user1, user);

    }


}
